package Homework5.Implementation;

import Homework5.Common.ObjectInputStreamWrapper;
import Homework5.Common.ObjectOutputStreamWrapper;
import Homework5.Model.Common.Exceptions.IncorrectEmployeeTypeException;
import Homework5.Model.Common.Interfaces.IEmployeeList;
import Homework5.Model.Common.Interfaces.ISerializer;
import Homework5.Model.Common.Interfaces.IEmployee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileSerializer implements ISerializer {
    private final String fileName;
    private IEmployeeList internalList;

    public FileSerializer(String fileName, IEmployeeList list) {
        this.fileName = fileName;
        this.internalList = list;
        loadList();
    }

    private boolean saveEmployeeList(){
        try (ObjectOutputStreamWrapper objectOutputStreamWrapper = new ObjectOutputStreamWrapper(this.fileName)) {
            objectOutputStreamWrapper.writeObject(internalList);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private void loadList() {
        try (ObjectInputStreamWrapper objectInputStreamWrapper = new ObjectInputStreamWrapper(this.fileName)) {
            internalList = (IEmployeeList) objectInputStreamWrapper.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean save(IEmployee employeeForSave) throws IncorrectEmployeeTypeException {
        internalList.addEmployee(employeeForSave);
        return saveEmployeeList();
    }

    @Override
    public boolean delete(IEmployee employeeForRemove) throws IncorrectEmployeeTypeException {
        if (internalList.contains(employeeForRemove)) {
            internalList.deleteEmployee(employeeForRemove);
            return saveEmployeeList();
        }
        return false;
    }

    @Override
    public IEmployee getByName(String name) {
        for (IEmployee employee : internalList.getEmployeesItems()) {
            if (employee.getName().equals(name)) {
                return employee;
            }
        }
        return null;
    }

    @Override
    public List<IEmployee> getByJob(String job) {
        List<IEmployee> result = new ArrayList<>();
        for (IEmployee employee : internalList.getEmployeesItems()) {
            if (employee.getJob().equals(job)) {
                result.add(employee);
            }
        }
        return result;
    }

    @Override
    public boolean saveOrUpdate(IEmployee employeeForSaveOrUpdate) throws IncorrectEmployeeTypeException {
        delete(employeeForSaveOrUpdate);
        return save(employeeForSaveOrUpdate);
    }

    @Override
    public boolean changeAllWork(String sourceJob, String targetJob) {
        List<IEmployee> employeesWithSuchWork = getByJob(sourceJob);
        int changedWorkCount = 0;

        for (IEmployee employee : employeesWithSuchWork) {
            employee.setJob(targetJob);
            changedWorkCount++;
        }

        return changedWorkCount == employeesWithSuchWork.size() && saveEmployeeList();
    }
}
