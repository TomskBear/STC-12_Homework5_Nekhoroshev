package Homework5.main.serializable;

import Homework5.Common.SerializationType;
import Homework5.Common.SerializersCreator;
import Homework5.Model.Common.Base.Employee;
import Homework5.Model.Common.Base.EmployeeList;
import Homework5.Model.Common.Exceptions.IncorrectEmployeeTypeException;
import Homework5.Model.Common.Interfaces.IEmployee;
import Homework5.Model.Common.Interfaces.IEmployeeList;
import Homework5.Model.Common.Interfaces.ISerializer;
import Homework5.main.common.Util;

public class Main {
    public static void main(String[] args) {
        IEmployeeList list = new EmployeeList();

        ISerializer fileSerializer = SerializersCreator.createSerializers(SerializationType.File, "serializableTest.dat", list);
        if (fileSerializer != null) {
            Employee ted = new Employee("Ted", 11000, "Collector");
            Employee tom = new Employee("Tom", 15000, "Artist");
            Employee mark = new Employee("Mark", 98000, "Mechanic");
            Employee creg = new Employee("Creg", 120000, "Broker");
            Util.saveEmployee(fileSerializer, ted);
            Util.saveEmployee(fileSerializer, tom);
            Util.saveEmployee(fileSerializer, mark);
            Util.saveEmployee(fileSerializer, creg);

            IEmployee gotTom = fileSerializer.getByName("Tom");
            try {
                if (fileSerializer.delete(gotTom)) {
                    assert (fileSerializer.getByName("Tom") != null);
                }
            } catch (IncorrectEmployeeTypeException ex) {
                ex.printStackTrace();
            }

            try {
                assert (!fileSerializer.saveOrUpdate(gotTom));
            } catch (IncorrectEmployeeTypeException ex) {
                ex.printStackTrace();
            }
        }

    }
}
