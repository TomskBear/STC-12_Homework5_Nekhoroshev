package Homework5.main.common;

import Homework5.Model.Common.Exceptions.IncorrectEmployeeTypeException;
import Homework5.Model.Common.Interfaces.IEmployee;
import Homework5.Model.Common.Interfaces.ISerializer;

public class Util {
    public static void saveEmployee(ISerializer serializer, IEmployee employee) {
        try {
            serializer.save(employee);
        } catch (IncorrectEmployeeTypeException ex) {
            ex.printStackTrace();
        }
    }
}
