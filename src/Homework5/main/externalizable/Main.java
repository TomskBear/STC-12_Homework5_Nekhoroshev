package Homework5.main.externalizable;

import Homework5.Common.SerializationType;
import Homework5.Common.SerializersCreator;
import Homework5.Model.Common.Exceptions.IncorrectEmployeeTypeException;
import Homework5.Model.Common.Interfaces.IEmployee;
import Homework5.Model.Common.Interfaces.IEmployeeList;
import Homework5.Model.Common.Interfaces.ISerializer;
import Homework5.Model.Externalizable.ExternalizableEmployee;
import Homework5.Model.Externalizable.ExternalizableEmployeeList;
import Homework5.main.common.Util;

class Main {
    public static void main(String[] args) {
        IEmployeeList list = new ExternalizableEmployeeList();

        ISerializer fileSerializer = SerializersCreator.createSerializers(SerializationType.File, "externalizableTest.dat", list);
        if (fileSerializer != null) {
            ExternalizableEmployee ted = new ExternalizableEmployee("Ted", 11000, "Collector");
            ExternalizableEmployee tom = new ExternalizableEmployee("Tom", 15000, "Artist");
            ExternalizableEmployee mark = new ExternalizableEmployee("Mark", 98000, "Mechanic");
            ExternalizableEmployee creg = new ExternalizableEmployee("Creg", 120000, "Broker");
            Util.saveEmployee(fileSerializer, ted);
            Util.saveEmployee(fileSerializer, tom);
            Util.saveEmployee(fileSerializer, mark);
            Util.saveEmployee(fileSerializer, creg);

            IEmployee gotTom = fileSerializer.getByName("Tom");
            try {
                if (fileSerializer.delete(gotTom)) {
                    assert (fileSerializer.getByName("Tom") != null);
                }
            } catch (IncorrectEmployeeTypeException ex) {
                ex.printStackTrace();
            }

            try {
                assert (!fileSerializer.saveOrUpdate(gotTom));
            } catch (IncorrectEmployeeTypeException ex) {
                ex.printStackTrace();
            }
        }

    }
}
