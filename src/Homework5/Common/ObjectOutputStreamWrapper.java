package Homework5.Common;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ObjectOutputStreamWrapper implements AutoCloseable{
    private FileOutputStream fileOutputStream;
    private ObjectOutputStream objectOutputStream;

    public ObjectOutputStreamWrapper(String fileName) throws IOException, SecurityException {
        this.fileOutputStream = new FileOutputStream(fileName);
        this.objectOutputStream = new ObjectOutputStream(fileOutputStream);
    }

    public void writeObject(Object objectToWrite) throws IOException{
        objectOutputStream.writeObject(objectToWrite);
    }

    @Override
    public void close() throws IOException {
        fileOutputStream.flush();
        fileOutputStream.close();
        objectOutputStream.flush();
        objectOutputStream.close();
    }
}
