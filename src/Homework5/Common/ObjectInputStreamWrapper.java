package Homework5.Common;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ObjectInputStreamWrapper implements AutoCloseable {
    private ObjectInputStream inputStream;
    private FileInputStream fileInputStream;


    public ObjectInputStreamWrapper(String fileName) throws IOException, SecurityException {
        this.fileInputStream = new FileInputStream(fileName);
        this.inputStream = new ObjectInputStream(fileInputStream);
    }

    public Object readObject() throws IOException, ClassNotFoundException{
        return inputStream.readObject();
    }

    @Override
    public void close() throws IOException {
        fileInputStream.close();
        inputStream.close();
    }
}
