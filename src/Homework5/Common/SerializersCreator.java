package Homework5.Common;

import Homework5.Model.Common.Interfaces.IEmployeeList;
import Homework5.Model.Common.Interfaces.ISerializer;
import Homework5.Implementation.FileSerializer;

public class SerializersCreator {
    public static ISerializer createSerializers(SerializationType typeOfSerializer, String fileName, IEmployeeList list) {
        switch (typeOfSerializer) {
            case File:
                return new FileSerializer(fileName, list);
            //have a lot of questions about that
            /*case Xml:
                return new XmlSerializer(fileName);
            case Json:
                return new JsonSerializer(fileName);*/
            default:
                assert(true);
                return null;
        }
    }
}
