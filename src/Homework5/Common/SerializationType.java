package Homework5.Common;

public enum SerializationType {
    File,
    Json,
    Xml
}
