package Homework5.Model.Externalizable;

import Homework5.Model.Common.Base.Employee;
import Homework5.Model.Common.Exceptions.NameAlreadyDefinedException;
import Homework5.Model.Common.Exceptions.ValueBelowZeroException;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class ExternalizableEmployee extends Employee implements Externalizable {

    public ExternalizableEmployee() {
    }

    public ExternalizableEmployee(String name, int salary, String job) {
        super(name, salary, job);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(getName());
        out.writeInt(getSalary());
        out.writeUTF(getJob());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException {
        try {
            setName(in.readUTF());
        } catch (NameAlreadyDefinedException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        try {
            setSalary(in.readInt());
        } catch (ValueBelowZeroException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }

        setJob(in.readUTF());
    }
}
