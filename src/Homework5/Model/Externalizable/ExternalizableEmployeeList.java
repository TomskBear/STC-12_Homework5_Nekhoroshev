package Homework5.Model.Externalizable;

import Homework5.Model.Common.Base.EmployeeList;
import Homework5.Model.Common.Exceptions.IncorrectEmployeeTypeException;
import Homework5.Model.Common.Interfaces.IEmployee;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class ExternalizableEmployeeList extends EmployeeList implements Externalizable {

    private static final String incorrectEmployeeTypeMessage = "Externalizable employee list can not contains not externalizable employees";

    public ExternalizableEmployeeList() {
        super();
    }

    private void checkEmployeeType(IEmployee employeeForCheck) throws IncorrectEmployeeTypeException {
        if (!(employeeForCheck instanceof Externalizable)) {
            throw new IncorrectEmployeeTypeException(incorrectEmployeeTypeMessage);
        }
    }

    @Override
    public void addEmployee(IEmployee employee) throws IncorrectEmployeeTypeException {
        checkEmployeeType(employee);
        super.addEmployee(employee);
    }

    @Override
    public boolean deleteEmployee(IEmployee employee) throws IncorrectEmployeeTypeException {
        checkEmployeeType(employee);
        return super.deleteEmployee(employee);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(getSize());
        for (IEmployee employee : getEmployeesItems()) {
            Externalizable externalizable = (Externalizable) employee;
            externalizable.writeExternal(out);
        }
        out.writeInt(getTotalSalary());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        int incomingSize = in.readInt();
        for (int i = 0; i < incomingSize; i++) {
            ExternalizableEmployee employee = new ExternalizableEmployee();
            employee.readExternal(in);
            try {
                addEmployee(employee);
            } catch (IncorrectEmployeeTypeException e) {
                e.printStackTrace();
            }
        }
        setTotalSalary(in.readInt());
    }
}
