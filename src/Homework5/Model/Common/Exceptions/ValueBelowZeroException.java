package Homework5.Model.Common.Exceptions;

public class ValueBelowZeroException extends Exception {

    private static final String messageBaseString = "You can not set value below zero.";

    public ValueBelowZeroException(String message) {
        super(messageBaseString + " " + message);
    }
}
