package Homework5.Model.Common.Exceptions;

public class IncorrectEmployeeTypeException extends Exception {
    private static final String internalMessage = "You can not add employees with this type to this list.";

    public IncorrectEmployeeTypeException(String message) {
        super(internalMessage + " " + message);
    }
}
