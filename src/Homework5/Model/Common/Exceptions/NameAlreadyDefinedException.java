package Homework5.Model.Common.Exceptions;

public class NameAlreadyDefinedException extends Exception {

    private static final String messageString = "It is impossible to define name twice";

    public NameAlreadyDefinedException() {
        super(messageString);
    }
}
