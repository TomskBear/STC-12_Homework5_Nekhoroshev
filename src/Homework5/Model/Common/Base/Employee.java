package Homework5.Model.Common.Base;

import Homework5.Model.Common.Exceptions.NameAlreadyDefinedException;
import Homework5.Model.Common.Exceptions.ValueBelowZeroException;
import Homework5.Model.Common.Interfaces.IEmployee;

import java.io.Serializable;
import java.util.Objects;

public class Employee implements IEmployee, Serializable {
    private String name;
    private int salary;
    private String job;

    public Employee() {

    }

    public Employee(String name, int salary, String job) {
        this.name = name;
        this.salary = salary;
        this.job = job;
    }

    public String getName() {
        return name;
    }

    /**
     * method checks wheather this person has name already or not
     *
     * @return True - name for this person is defined, False - name not defined yet;
     */
    private boolean isCurrentNameDefined() {
        return this.name != null && !this.name.isEmpty();
    }

    /**
     * Sets the name of Person. If name already defined it thows Exception
     * @throws NameAlreadyDefinedException - this exception is thowing when name already defined for this person
     * @param name - name of person
     */
    public void setName(String name) throws NameAlreadyDefinedException {
        if (this.name != null && !this.name.isEmpty()) {
            throw new NameAlreadyDefinedException();
        }

        if (name != null && !name.isEmpty() && !isCurrentNameDefined()) {
            this.name = name;
        }
    }

    public int getSalary() {
        return salary;
    }

    /**
     * method sets salary for the Person. If incoming value is below zero it throws Exception
     * @throws ValueBelowZeroException - this exception is throwing when somebody tries to set salary below zero
     * @param salary - new salary value for this Person
     */
    public void setSalary(int salary) throws ValueBelowZeroException {
        if (salary < 0) {
            throw new ValueBelowZeroException("Salary value is below zero");
        }
        this.salary = salary;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return salary == employee.salary &&
                Objects.equals(name, employee.name) &&
                Objects.equals(job, employee.job);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, salary, job);
    }
}
