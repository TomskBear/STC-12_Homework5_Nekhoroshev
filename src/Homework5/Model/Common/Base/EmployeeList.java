package Homework5.Model.Common.Base;

import Homework5.Model.Common.Exceptions.IncorrectEmployeeTypeException;
import Homework5.Model.Common.Interfaces.IEmployee;
import Homework5.Model.Common.Interfaces.IEmployeeList;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

public class EmployeeList implements IEmployeeList, Serializable {
    private HashSet<IEmployee> employees;
    private int totalSalary;

    public EmployeeList() {
        employees = new HashSet<>();
        this.totalSalary = 0;
    }


    public boolean contains(IEmployee employee) {
        return employees.contains(employee);
    }

    /**
     * returns size of internal collection with employees
     * @return - int value
     */
    public int getSize() {
        return employees.size();
    }

    /**
     * get all employees list
     * @return - HashSet with employees list
     */
    public Collection<IEmployee> getEmployeesItems() {
        return employees;
    }


    /**
     * set new employees list
     * @param items for list
     */
    public void setEmployeesItems(Collection<IEmployee> items) {
        this.employees = (HashSet<IEmployee>) items;
    }

    /**
     * add new employee
     * @param employee - new Employee
     */
    public void addEmployee(IEmployee employee) throws IncorrectEmployeeTypeException {
        if (employee != null) {
            employees.add(employee);
            totalSalary += employee.getSalary();
        }
    }

    /**
     * method removes employee if internal list contains it.
     *
     * @param employee - employee for remove
     * @return True - employee removed, False - employee not removed
     * @throws IncorrectEmployeeTypeException - this is a base class, so we need to declare exception here for correct overriding
     */
    public boolean deleteEmployee(IEmployee employee) throws IncorrectEmployeeTypeException {
        boolean isRemoved = employees.remove(employee);
        if (isRemoved) {
            totalSalary -= employee.getSalary();
        }
        return isRemoved;
    }

    /**
     * method returns total salary for all employees
     * @return
     */
    public int getTotalSalary() {
        return this.totalSalary;
    }

    /**
     * method sets total salary for all employees. This method is needed only in deserialization method
     * @param totalSalary
     */
    protected void setTotalSalary(int totalSalary) {
        this.totalSalary = totalSalary;
    }
}
