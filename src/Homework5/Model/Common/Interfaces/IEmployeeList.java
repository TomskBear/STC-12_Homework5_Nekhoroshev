package Homework5.Model.Common.Interfaces;

import Homework5.Model.Common.Exceptions.IncorrectEmployeeTypeException;

import java.util.Collection;

public interface IEmployeeList {

    /**
     * get all employees
     *
     * @return - return HashSet with employees
     */
    Collection<IEmployee> getEmployeesItems();

    /**
     * set new employees list
     */
    void setEmployeesItems(Collection<IEmployee> items);

    /**
     * method checks does this list contains such employee
     *
     * @param employee - specified employee
     * @return - True - contains, False - does not contains
     */
    boolean contains(IEmployee employee);

    /**
     * method returns total salary for all employees
     *
     * @return integer value with sum
     */
    int getTotalSalary();

    /**
     * add new employee to list if it does not contains such employee
     *
     * @param employee - new Employee
     */
    void addEmployee(IEmployee employee) throws IncorrectEmployeeTypeException;

    /**
     * method removes employee if internal list contains it.
     *
     * @param employee - employee for remove
     * @return True - employee removed, False - employee not removed
     * @throws IncorrectEmployeeTypeException
     */
    boolean deleteEmployee(IEmployee employee) throws IncorrectEmployeeTypeException;
}
