package Homework5.Model.Common.Interfaces;

import Homework5.Model.Common.Exceptions.IncorrectEmployeeTypeException;
import java.util.List;

public interface ISerializer {
    /**
     * Save Employee to end of data
     * @param employeeForSave - employee to save
     * @return True - employee successfully saved, False - employee not saved
     */
    boolean save(IEmployee employeeForSave) throws IncorrectEmployeeTypeException;

    /**
     * Removes Employee from data
     * @param employeeForRemove - employee which must be removed if exists
     * @return True - Emplyee removed, False - Employee not removed
     */
    boolean delete(IEmployee employeeForRemove) throws IncorrectEmployeeTypeException;

    /**
     * Returns Employee by name, we expect that all Employees has unique names
     * @param name - name of Employee
     * @return - Employee with required name. If there is no such employee function returns null.
     */
    IEmployee getByName(String name);

    /**
     * Returns list of Employees with specified job
     * @param job - job for search Employees with such work
     * @return list of Employees with specified job
     */
    List<IEmployee> getByJob(String job);

    /**
     * Method appends Employee to data if it did not exist there before and update info about it if it exists
     * @param employeeForSaveOrUpdate - new info about employee
     * @return True - Employee saved or updated successfully, False - Employee did not saved and updated
     */
    boolean saveOrUpdate(IEmployee employeeForSaveOrUpdate) throws IncorrectEmployeeTypeException;

    /**
     * Method changes all work from sourceJob to targetJob for such Employees, which has job equal to sourceJob
     * @param sourceJob - source job value for search in Employees list
     * @param targetJob -  target job value which must be set for Employees with job.equals(sourceJob)
     * @return True - all employees with source job change it to targetJob
     */
    boolean changeAllWork(String sourceJob, String targetJob);
}
