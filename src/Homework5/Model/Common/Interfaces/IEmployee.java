package Homework5.Model.Common.Interfaces;

import Homework5.Model.Common.Exceptions.NameAlreadyDefinedException;
import Homework5.Model.Common.Exceptions.ValueBelowZeroException;

public interface IEmployee {

    /**
     * method gets name of employee
     *
     * @return - string with name
     */
    String getName();

    /**
     * Sets the name of Person. If name already defined it thows Exception
     *
     * @param name - name of person
     * @throws NameAlreadyDefinedException
     */
    void setName(String name) throws NameAlreadyDefinedException;

    /**
     * method gets salary of employee
     *
     * @return - value with salary
     */
    int getSalary();

    /**
     * method sets salary for the Person. If incoming value is below zero it throws Exception
     *
     * @param salary - new salary value for this Person
     * @throws ValueBelowZeroException
     */
    void setSalary(int salary) throws ValueBelowZeroException;

    /**
     * method gets current job of employee
     *
     * @return string with job name
     */
    String getJob();

    /**
     * method sets current job of employee
     */
    void setJob(String job);
}
